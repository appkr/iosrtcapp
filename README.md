Forked version of [iOSRTCApp](https://github.com/eface2face/iOSRTCApp)

## 빌드 및 사용법

- 필요 툴 설치:
```bash
$ npm install cordova xcode -g
```

- 소스코드 다운로드:
```bash
$ git clone git@bitbucket.org:appkr/iosrtcapp.git
```

- Node 패키지 설치:
```bash
$ cd iosrtcapp && npm install
// 에러나면, sudo npm install
```

- 플랫폼 및 플러그인 추가:
```bash
$ cordova platform add ios android
$ cordova plugin add cordova-plugin-whitelist cordova-plugin-crosswalk-webview cordova-plugin-camera cordova-plugin-console https://github.com/apache/cordova-plugin-device https://github.com/eface2face/cordova-plugin-iosrtc
```

- 폰에 탑재하고 실행:
```bash
$ cordova run android --device
$ cordova run ios --device
```

- https://apprtc.appspot.com/ 에서 채널 만들고, 모바일에서 그 채널 번호 입력!

- Android 디버그
  - Android USB 케이블 연결
  - Desktop Chrome에서 `chrome://inspect` 주소 입력
  - 또는 Desktop console에서 `adb logcat | grep 'Cordova \| iOSRTCApp \| anyString'`

- iOS 디버그
  - Xcode 콘솔

## CREDIT

*AppRTC* code is owned by Google as stated in the original [LICENSE](LICENSE.md) file.

Changes to the original *AppRTC* HTML5 source code (to become a Cordova iOS application) are written by Iñaki Baz Castillo at [eFace2Face, inc](https://eface2face.com).